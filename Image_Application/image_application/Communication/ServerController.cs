﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Image_Application.Classes;
using Newtonsoft.Json;
using Image_Application.Monitoring;
using System.ComponentModel;

namespace Image_Application.Communication
{
    public class ServerController
    {

        public static bool HttpCreateObject(ObjectClass item)
        {
            /// 
            /// Takes in the item after it has been saved to local storage
            /// Can get all information from the item, including image locations
            /// Upload images, get response where they get uploaded to save in database
            /// 

            string[] serverPaths = new string[4] { "", "", "", "" };   //Array to save server paths for the secondary upload
            int counter = item.imagePaths.Length;   //Counter for imagePaths length
            //Send Images to server
            for (int cnt = 0; cnt < counter; cnt++)
            {
                if (item.imagePaths[cnt] != "")
                {
                    serverPaths[cnt] = FileToBackend(new FileInfo(item.imagePaths[cnt]), item);
                    //Check if something failed
                    if (serverPaths[cnt] == "Error")
                    {
                        Logger.DisplayAlert("Post Object Error", "Image upload failed", "Ok");
                        return false;
                    }
                }
            }

            //Upload information to server
            //string postFileUrl = "https://swipq-dev.fi.cloudplatform.fi/api/uploadFull";
            string postFileUrl = "http://192.168.1.6:80/api/uploadFull/";

            return ObjectToBackend(item, postFileUrl, serverPaths);
        }

        public static string FileToBackend(FileInfo fi, ObjectClass item)
        {
            //string postFileUrl = "https://swipq-dev.fi.cloudplatform.fi/api/upload";
            string postFileUrl = "http://192.168.1.6:80/api/upload";
            string mimeType = "";
            string fileExt = fi.Extension.ToLower();

            if (fileExt.Contains("raw") == true)
            {
                mimeType = "application/octet-stream";
            }
            else if (fileExt.Contains("pdf") == true)
            {
                mimeType = "application/pdf";
            }
            else if (fileExt.Contains("jpg") == true)
            {
                mimeType = "image/jpeg";
            }
            else if (fileExt.Contains("jpeg") == true)
            {
                mimeType = "image/jpeg";
            }
            else if (fileExt.Contains("txt") == true)
            {
                mimeType = "txt";
            }

            NameValueCollection nvc = new NameValueCollection();
            nvc.Add("id", "TTR");
            nvc.Add("uuid", fi.Name.Split('.')[0]);       //Image name that is also the uniqe identifier
            nvc.Add("userid", item.userid); //Unique ID of user
            try
            {
                return HttpUploadFileAsync(postFileUrl, fi.FullName, "file", mimeType, nvc).Result;
            }
            catch (Exception exc)
            {
                Logger.DisplayAlert("Server Controller Error", exc.Message, "Ok");
                return @"Error/" + exc.Message;
            }
        }

        public static async Task<string> HttpUploadFileAsync(string url, string file, string paramName, string contentType, NameValueCollection nvc)
        {
            string boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x");
            byte[] boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");

            HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(url);
            wr.ContentType = "multipart/form-data; boundary=" + boundary;
            wr.Method = "POST";
            wr.KeepAlive = true;
            wr.Credentials = CredentialCache.DefaultCredentials;

            Stream rs = await wr.GetRequestStreamAsync().ConfigureAwait(false);

            string formdataTemplate = "Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}";
            foreach (string key in nvc.Keys)
            {
                rs.Write(boundarybytes, 0, boundarybytes.Length);
                string formitem = string.Format(formdataTemplate, key, nvc[key]);
                byte[] formitembytes = System.Text.Encoding.UTF8.GetBytes(formitem);
                rs.Write(formitembytes, 0, formitembytes.Length);
            }
            rs.Write(boundarybytes, 0, boundarybytes.Length);

            string headerTemplate = "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: {2}\r\n\r\n";
            string[] arguments = new string[] { paramName, file, contentType };
            string header = string.Format(headerTemplate, arguments);
            byte[] headerbytes = System.Text.Encoding.UTF8.GetBytes(header);
            rs.Write(headerbytes, 0, headerbytes.Length);

            FileStream fileStream = new FileStream(file, FileMode.Open, FileAccess.Read);
            byte[] buffer = new byte[4096];
            int bytesRead = 0;
            while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
            {
                rs.Write(buffer, 0, bytesRead);
            }
            fileStream.Close();

            byte[] trailer = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "--\r\n");
            rs.Write(trailer, 0, trailer.Length);
            rs.Close();

            WebResponse wresp = null;
            string bSuccess = "Failed";
            try
            {
                wresp = await wr.GetResponseAsync().ConfigureAwait(false);
                Stream stream2 = wresp.GetResponseStream();
                StreamReader reader2 = new StreamReader(stream2);
                string response = reader2.ReadToEnd();

                //Close all things
                stream2.Close();
                reader2.Close();
                wresp.Close();
                wresp = null;

                bSuccess = response;
            }
            catch (Exception ex)
            {
                Logger.DisplayAlert("Sending error", "An exception occurred: " + ex.Message, "Ok");
                
                if (wresp != null)
                {
                    wresp.Close();
                    wresp = null;
                }
                bSuccess = @"Error/" + ex.Message;
            }
            finally
            {
                wr = null;
            }

            return bSuccess;
        }

        public static bool HttpDeleteObject(ObjectClass item)
        {
            if (!CheckIfObjectExsists(item.uuid))
                return true;

            //string postFileUrl = "https://swipq-dev.fi.cloudplatform.fi/api/deleteInfo/";
            string postFileUrl = "http://192.168.1.6:80/api/deleteInfo/";

            HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(postFileUrl + item.uuid);
            wr.Method = "POST";
            wr.KeepAlive = true;
            wr.Credentials = CredentialCache.DefaultCredentials;

            WebResponse wresp = null;
            bool bSuccess = false;
            try
            {
                wresp = wr.GetResponse();
                Stream stream2 = wresp.GetResponseStream();
                StreamReader reader2 = new StreamReader(stream2);
                string response = reader2.ReadToEnd();

                //Close all things
                stream2.Close();
                reader2.Close();
                wresp.Close();
                wresp = null;

                bSuccess = true;
            }
            catch (Exception ex)
            {
                Logger.DisplayAlert("Sending error", "An exception occurred: " + ex.Message, "Ok");

                //log.Error("Error uploading file", ex);
                if (wresp != null)
                {
                    wresp.Close();
                    wresp = null;
                }
                bSuccess = false;
            }
            finally
            {
                wr = null;
            }

            return bSuccess;
        }

        public static bool HttpDeleteImage(string imageID)
        {
            string response = CheckIfImageExists(imageID);
            if (response == "not found")
                return true;
            
            //string postFileUrl = "https://swipq-dev.fi.cloudplatform.fi/api/deleteImage/";
            string postFileUrl = "http://192.168.1.6:80/api/deleteImage/";

            HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(postFileUrl + imageID);
            wr.Method = "POST";
            wr.KeepAlive = true;
            wr.Credentials = CredentialCache.DefaultCredentials;

            WebResponse wresp = null;
            bool bSuccess = false;
            try
            {
                wresp = wr.GetResponse();
                Stream stream2 = wresp.GetResponseStream();
                StreamReader reader2 = new StreamReader(stream2);
                response = reader2.ReadToEnd();

                //Close all things
                stream2.Close();
                reader2.Close();
                wresp.Close();
                wresp = null;

                bSuccess = true;
            }
            catch (Exception ex)
            {
                Logger.DisplayAlert("Sending error", "An exception occurred: " + ex.Message, "Ok");

                //log.Error("Error uploading file", ex);
                if (wresp != null)
                {
                    wresp.Close();
                    wresp = null;
                }
                bSuccess = false;
            }
            finally
            {
                wr = null;
            }

            return bSuccess;
        }

        public static bool HttpEditObject(ObjectClass item)
        {
            if (!CheckIfObjectExsists(item.uuid))
                return true;

            string[] serverPaths = new string[4] { "", "", "", "" };   //Array to save server paths for the secondary upload
            int counter = item.imagePaths.Length;   //Counter for imagePaths length
            //Send Images to server
            for (int cnt = 0; cnt < counter; cnt++)
            {
                if (item.imagePaths[cnt] != "")
                {
                    //Check if image already exsist on the backend
                    FileInfo file = new FileInfo(item.imagePaths[cnt]);
                    string response = CheckIfImageExists(file.Name.Split('.')[0]);
                    if (response == "not found")
                    {
                        //Fixing the string
                        serverPaths[cnt] = FileToBackend(new FileInfo(item.imagePaths[cnt]), item);
                        //Check if something failed
                        if (serverPaths[cnt] == "Error")
                        {
                            Logger.DisplayAlert("Post Object Error", "Image upload failed", "Ok");
                            return false;
                        }
                    }
                    else
                        serverPaths[cnt] = response;
                }
            }

            //Upload information to server
            //string postFileUrl = "https://swipq-dev.fi.cloudplatform.fi/api/editInfo/";
            string postFileUrl = "http://192.168.1.6:80/api/editInfo/";

            return ObjectToBackend(item, postFileUrl, serverPaths);
        }

        public static bool ObjectToBackend(ObjectClass item, string postFileUrl, string[] serverPaths)
        {
            string boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x");
            byte[] boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");

            HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(postFileUrl);
            wr.ContentType = "multipart/form-data; boundary=" + boundary;
            wr.Method = "POST";
            wr.KeepAlive = true;
            wr.Credentials = CredentialCache.DefaultCredentials;

            Stream rs = wr.GetRequestStream();

            NameValueCollection nvc = new NameValueCollection();
            nvc.Add("uuid", item.uuid);
            nvc.Add("userid", item.userid);
            nvc.Add("title", item.Title);
            nvc.Add("category", item.Category);
            nvc.Add("description", item.Description);
            nvc.Add("path1", serverPaths[0]);
            nvc.Add("path2", serverPaths[1]);
            nvc.Add("path3", serverPaths[2]);
            nvc.Add("path4", serverPaths[3]);

            string formdataTemplate = "Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}";
            foreach (string key in nvc.Keys)
            {
                rs.Write(boundarybytes, 0, boundarybytes.Length);
                string formitem = string.Format(formdataTemplate, key, nvc[key]);
                byte[] formitembytes = System.Text.Encoding.UTF8.GetBytes(formitem);
                rs.Write(formitembytes, 0, formitembytes.Length);
            }
            rs.Write(boundarybytes, 0, boundarybytes.Length);

            byte[] trailer = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "--\r\n");
            rs.Write(trailer, 0, trailer.Length);
            rs.Close();

            WebResponse wresp = null;
            bool bSuccess = false;
            try
            {
                wresp = wr.GetResponse();
                Stream stream2 = wresp.GetResponseStream();
                StreamReader reader2 = new StreamReader(stream2);
                string response = reader2.ReadToEnd();

                //Close all things
                stream2.Close();
                reader2.Close();
                wresp.Close();
                wresp = null;

                bSuccess = true;
            }
            catch (Exception ex)
            {
                Logger.DisplayAlert("Sending error", "An exception occurred: " + ex.Message, "Ok");

                //log.Error("Error uploading file", ex);
                if (wresp != null)
                {
                    wresp.Close();
                    wresp = null;
                }
                bSuccess = false;
            }
            finally
            {
                wr = null;
            }

            return bSuccess;
        }

        static bool CheckIfObjectExsists(string uuid)
        {
            //Check if uuid got a value, if not return
            if (uuid == "" || uuid == null)
                return false;

            try
            {
                //Check if file already exsist on the backend
                HttpWebRequest wr = (HttpWebRequest)WebRequest.Create("http://192.168.1.6:80/api/checkInfo/" + uuid);
                wr.Method = "GET";
                WebResponse wresp = wr.GetResponse();
                Stream stream2 = wresp.GetResponseStream();
                StreamReader reader2 = new StreamReader(stream2);
                string response = reader2.ReadToEnd();

                //Close all things
                wresp.Close();
                stream2.Close();
                reader2.Close();

                if (response == "exists")
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                Logger.DisplayAlert("Check Error", ex.Message, "Ok");
                return false;
            }
        }

        static string CheckIfImageExists(string uuid)
        {
            //Check if uuid got a value, if not return
            if (uuid == "" || uuid == null)
                return "";

            try
            {
                //Check if file already exsist on the backend
                HttpWebRequest wr = (HttpWebRequest)WebRequest.Create("http://192.168.1.6:80/api/checkImage/" + uuid);
                wr.Method = "GET";
                WebResponse wresp = wr.GetResponse();
                Stream stream2 = wresp.GetResponseStream();
                StreamReader reader2 = new StreamReader(stream2);
                string response = reader2.ReadToEnd();

                //Close all things
                wresp.Close();
                stream2.Close();
                reader2.Close();

                return response;
            }
            catch (Exception ex)
            {
                Logger.DisplayAlert("Check Image Error", ex.Message, "Ok");
                return "Error";
            }
        }

        public static async Task<ObjectClass[]> GetItemList()
        {
            List<ObjectClass> items = new List<ObjectClass>();

            //string postFileUrl = "https://swipq-dev.fi.cloudplatform.fi/api/listInfo/";
            string postFileUrl = "http://192.168.1.6:80/api/listInfo/";

            HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(postFileUrl);
            wr.Method = "GET";
            wr.KeepAlive = true;
            wr.Credentials = CredentialCache.DefaultCredentials;

            WebResponse wresp = null;
            try
            {
                wresp = await wr.GetResponseAsync().ConfigureAwait(false);
                Stream stream2 = wresp.GetResponseStream();
                StreamReader reader2 = new StreamReader(stream2);
                string response = reader2.ReadToEnd();

                SimpleObjectClass[] tempItems = JsonConvert.DeserializeObject<SimpleObjectClass[]>(response);

                //Close all things
                stream2.Close();
                reader2.Close();
                wresp.Close();
                wresp = null;

                foreach (SimpleObjectClass temp in tempItems)
                    items.Add(new ObjectClass(temp));
            }
            catch (Exception ex)
            {
                Logger.DisplayAlert("Sending error", "An exception occurred: " + ex.Message, "Ok");

                //log.Error("Error uploading file", ex);
                if (wresp != null)
                {
                    wresp.Close();
                    wresp = null;
                }
            }
            finally
            {
                wr = null;
            }

            return items.ToArray();
        }

        //Download an image to the Object class sent in
        public static bool DownloadImages(ObjectClass item)
        {
            try
            {
                //Loop through all image paths
                for (int i = 0; i < item.imagePaths.Length; i++)
                {
                    if(item.imagePaths[i] != "")
                        item.imagePaths[i] = DownloadImage(item.imagePaths[i]);
                }

                return true;
            }
            catch (Exception ex)
            {
                Logger.DisplayAlert("Download Error", ex.Message, "Ok");
                return false;
            }
        }

        //Get uuid, download image and return path to the image
        public static string DownloadImage(string uuid)
        {
            string response = CheckIfImageExists(uuid);
            if (response == "not found")
                return "";  //return empty string if no image was found. So it doesn't try to load the image

            //string postFileUrl = "https://swipq-dev.fi.cloudplatform.fi/api/download/";
            string postFileUrl = "http://192.168.1.6:80/api/download/";
            string workingDirectory = Path.Combine(Path.GetTempPath(), uuid + ".jpeg");

            //Check if the file already exsists in the folder
            if (File.Exists(workingDirectory))
                return workingDirectory;    //Return exsisting path

            try
            {
                WebClient webClient = new WebClient();
                webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(webClient_DownloadFileCompleted);
                webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(webClient_DownloadProgressChanged);
                webClient.DownloadFile(new Uri(postFileUrl + uuid), workingDirectory);

                return workingDirectory;
            }
            catch (Exception ex)
            {
                Logger.DisplayAlert("Sending error", "An exception occurred: " + ex.Message, "Ok");
                return "";  //return empty string if no image was found. So it doesn't try to load the image
            }
        }

        static void webClient_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
        }

        static void webClient_DownloadProgressChanged(object sender, System.Net.DownloadProgressChangedEventArgs e)
        {
        }
    }
}
