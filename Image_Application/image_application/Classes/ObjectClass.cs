﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Image_Application.Classes
{
    public class ObjectClass : IEquatable<ObjectClass>
    {
        public string userid;
        public string uuid;
        public string Title { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public string[] imagePaths;

        //Default constructor used for loading from file
        public ObjectClass()
        {
            imagePaths = new string[4] { "", "", "", "" };
        }

        public ObjectClass(string userid)
        {
            this.userid = userid;
            imagePaths = new string[4] { "", "", "", "" };
        }

        //Constructor used when taking info from backend
        public ObjectClass(SimpleObjectClass item)
        {
            userid = item.userid;
            uuid = item.uuid;
            Title = item.title;
            Category = item.category;
            Description = item.description;

            imagePaths = new string[4] { "", "", "", "" };
            if(item.path1 != null)
                imagePaths[0] = item.path1;
            if (item.path2 != null)
                imagePaths[1] = item.path2;
            if (item.path3 != null)
                imagePaths[2] = item.path3;
            if (item.path4 != null)
                imagePaths[3] = item.path4;
        }

        public bool Equals(ObjectClass other)
        {
            return uuid == other.uuid && userid == other.userid;
        }
    }
}
