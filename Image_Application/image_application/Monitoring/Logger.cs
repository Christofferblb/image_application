﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Image_Application.Monitoring
{
    public static class Logger
    {
        static MainPage main;
        public static MainPage Main { get { return main; } set { main = value; } }

        public static void DisplayAlert(string title, string content, string button)
        {
            Main.DisplayAlert(title, content, button);
        }
    }
}
