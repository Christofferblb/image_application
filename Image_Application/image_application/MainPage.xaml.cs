﻿/// 
/// The MIT License (MIT)
/// Copyright(c) 2007 James Newton-King
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
/// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, 
/// sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so
/// 

using System;
using System.Collections.Generic;
using System.ComponentModel;
using Xamarin.Forms;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System.Collections.ObjectModel;
using Newtonsoft.Json;
using System.IO;
using Xamarin.Essentials;
using Rg.Plugins.Popup;
using Image_Application.Classes;
using Image_Application.Communication;
using Image_Application.Monitoring;

namespace Image_Application
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        static int numberOfImages = 4;
        //All items
        public ObservableCollection<ObjectClass> Items { get; set; }
        string userID;
        //Visuals
        ActivityIndicator loading;
        Button captureButton;

        public MainPage()
        {
            //Define an unique user ID if it does not already exist
            string xmlPath = Preferences.Get("ID_String", string.Empty);
            if (string.IsNullOrWhiteSpace(xmlPath))
            {
                string id = Guid.NewGuid().ToString();
                Preferences.Set("ID_String", id);
            }

            userID = Preferences.Get("ID_String", "unique");

            //Setting up activityIndicator for loading
            loading = new ActivityIndicator { IsRunning = false, IsVisible = false, IsEnabled = false, Color = Color.DarkMagenta, HorizontalOptions = LayoutOptions.FillAndExpand, VerticalOptions = LayoutOptions.FillAndExpand };
            //Setting up CaptureButton
            captureButton = new Button { FontSize = 72, Text = "+", MinimumHeightRequest = 80, HeightRequest = 150, MinimumWidthRequest = 80, WidthRequest = 150 };
            captureButton.Clicked += delegate { TakePicture(currentClass); };

            //If it fails to create or get the unique id it should not start;
            if (userID != "unique")
            {
                Logger.Main = this; //Give the mainpage to the Logger
                HomePage(); //Initialize the Home page
            }
            else
                DisplayAlert("Unique ID error", "Failed to create or get the unique ID, please restart or reinstall the application.", "Ok");

            InitializeComponent();
        }

        void HomePage()
        {
            Label pageTitle = new Label { Text = "Objects", VerticalOptions = LayoutOptions.Start, HorizontalOptions = LayoutOptions.CenterAndExpand };

            ListView objectList = new ListView { HorizontalOptions = LayoutOptions.FillAndExpand, VerticalOptions = LayoutOptions.FillAndExpand, BackgroundColor = Color.LightSlateGray };
            PopulateObjectList(objectList);

            Button newObjectButton = new Button { Text = "New Object" };
            newObjectButton.Clicked += delegate { BuildItemPage(); };

            StackLayout homeLayout = new StackLayout();
            homeLayout.Children.Add(pageTitle);
            homeLayout.Children.Add(objectList);
            homeLayout.Children.Add(newObjectButton);

            Content = homeLayout;
        }

        async void PopulateObjectList(ListView objectList)
        {
            Items = new ObservableCollection<ObjectClass>(LoadFromFile());

            ObjectClass[] otherItems = await ServerController.GetItemList();
            foreach(ObjectClass tempItem in otherItems)
            {
                if (!Items.Contains(tempItem))
                {
                    //Add to both ObjectClass lists
                    Items.Add(tempItem);
                }
            }

            objectList.ItemsSource = Items;                               //Set listview items

            var template = new DataTemplate(typeof(TextCell));
            template.SetBinding(TextCell.TextProperty, "Title");         //Set template title
            template.SetBinding(TextCell.DetailProperty, "Category");    //Set template details
            objectList.ItemTemplate = template;                             //Add template to listview
                                                                            //objectList.ItemsSource = Objects as IEnumerable;              //IEnumerable for search?

            objectList.ItemTapped += ItemTapped;
        }

        List<ObjectClass> LoadFromFile()
        {
            List<ObjectClass> items = new List<ObjectClass>();

            //Load the files
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            //FilePath = /data/user/0/com.companyname.CameraTesting/files/cameratest.txt
            string filePath = Path.Combine(path, "cameratest.txt");

            /*if (File.Exists(filePath))
                File.Delete(filePath);*/

            if (File.Exists(filePath))
            {
                var file = File.Open(filePath, FileMode.Open, FileAccess.Read);
                using (var strm = new StreamReader(file))
                {
                    string line;
                    while ((line = strm.ReadLine()) != null)
                    {
                        try
                        {
                            ObjectClass itemFromFile = JsonConvert.DeserializeObject<ObjectClass>(line);
                            items.Add(itemFromFile);
                        }
                        catch (Exception e)
                        {
                            DisplayAlert("Load", "Could not load file. " + e.Message, "Ok");
                        }
                    }
                }
            }

            return items;
        }

        public void ItemTapped(object sender, ItemTappedEventArgs e)
        {
            ListView myListView = (ListView)sender;
            ObjectClass myItem = (ObjectClass)myListView.SelectedItem;
            for(int cnt = 0; cnt < Items.Count; cnt++)
            {
                if(Items[cnt] == myItem)
                {
                    BuildItemPage(cnt);
                    return;
                }
            }
            //await DisplayAlert("Opening", "Opening selected item", "Open");
            
        }

        Grid imageGrid;
        ObjectClass currentClass;
        InformationFields infoFields;
        struct InformationFields
        {
            public Entry titleEntry;
            public Picker categoryPicker;
            public Editor descriptionEditor;
        }

        void BuildItemPage(int itemIndex = -1)
        {
            if (itemIndex > -1)
                currentClass = Items[itemIndex];
            else
                currentClass = new ObjectClass(userID);

            Button backButton = new Button { Text = "< Back"};
            backButton.Clicked += delegate { HomePage(); };

            infoFields = new InformationFields();

            infoFields.titleEntry = new Entry { Placeholder = "Golf club, monitor, etc...."};

            infoFields.categoryPicker = new Picker { Title = "Category" };
            infoFields.categoryPicker.ItemsSource = new string[] {"Sports", "Furnishing", "Pets", "Technology", "Other" };

            infoFields.descriptionEditor = new Editor { MinimumHeightRequest = 100, HeightRequest = 200, Placeholder = "Describe your item..."};

            imageGrid = new Grid { MinimumHeightRequest = 200, HeightRequest = 400 };
            imageGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            imageGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            imageGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            imageGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

            if (currentClass.userid == userID)
                imageGrid.Children.Add(captureButton, 0, 0);

            //Fill information if there is information to fill
            if (currentClass.Title != null)
                infoFields.titleEntry.Text = currentClass.Title;
            
            if (currentClass.Category != null)
                infoFields.categoryPicker.SelectedItem = currentClass.Category;

            if (currentClass.Description != null)
                infoFields.descriptionEditor.Text = currentClass.Description;

            if (currentClass.imagePaths != null)
            {
                //If it's a class from the server, start by checking the paths and download the files
                if(currentClass.userid != userID)
                    ServerController.DownloadImages(currentClass);

                foreach (string s in currentClass.imagePaths)
                {
                    if (s != "")
                        ArrangeImageGrid(new Image { Source = s });
                }
            }

            StackLayout content = new StackLayout();
            content.Children.Add(backButton);
            content.Children.Add(infoFields.titleEntry);
            content.Children.Add(infoFields.categoryPicker);
            content.Children.Add(infoFields.descriptionEditor);
            content.Children.Add(imageGrid);

            //Display Delete & Save button if the item is owned by current user
            if (currentClass.userid == userID)
            {
                //Save button
                Button saveButton = new Button { Text = "Save" };
                saveButton.Clicked += delegate { SaveObject(currentClass, itemIndex); };
                content.Children.Add(saveButton);

                if (itemIndex >= 0)
                {
                    //Delete button
                    Button deleteButton = new Button { Text = "Delete" };
                    deleteButton.Clicked += delegate { DeleteItem(itemIndex); };
                    content.Children.Add(deleteButton);

                    //Save button text to save changes
                    saveButton.Text = "Save Changes";
                }
            }

            //Set the final layout
            Grid itemPage = new Grid();
            itemPage.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            itemPage.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(2, GridUnitType.Auto) });
            itemPage.Children.Add(loading, 1, 0);
            itemPage.Children.Add(content, 0, 0);
            ScrollView pageContent = new ScrollView { Content = itemPage, VerticalScrollBarVisibility = ScrollBarVisibility.Always};
            Content = pageContent;
        }

        void SaveObject(ObjectClass item, int itemIndex)
        {
            if (infoFields.titleEntry.Text == "" || infoFields.categoryPicker.SelectedItem == null)
            {
                DisplayAlert("Error saving", "No title or category was filled in.", "Ok");
                return;
            }

            ToggleLoad();

            //Fill the last information to the ObjectClass
            item.userid = userID;
            if (item.uuid == null)
                item.uuid = Guid.NewGuid().ToString();
            item.Title = infoFields.titleEntry.Text;
            item.Category = infoFields.categoryPicker.SelectedItem.ToString();
            item.Description = infoFields.descriptionEditor.Text;

            if (itemIndex < 0)
            {
                Items.Add(item);

                //Send file to servers
                if (!ServerController.HttpCreateObject(item))
                    return;
            }
            else
            {
                if (!ServerController.HttpEditObject(item))
                    return;
            }

            //Save locally
            SaveLocally();

            if(itemIndex < 0)
            {
                //DisplayAlert("Saving", "This is a non working save function", "Ok");
                HomePage(); //Has to be after local save
            }

            ToggleLoad();
        }

        void SaveLocally()
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            //FilePath = /data/user/0/com.companyname.CameraTesting/files/cameratest.txt
            string filePath = Path.Combine(path, "cameratest.txt");

            if (File.Exists(filePath))
                File.Delete(filePath);

            //Save the file locally
            foreach (ObjectClass obj in Items)
            {
                if (obj.userid == userID)
                {
                    string json = JsonConvert.SerializeObject(obj);
                    var file = File.Open(filePath, FileMode.Append, FileAccess.Write);
                    using (var strm = new StreamWriter(file))
                    {
                        strm.WriteLine(json);
                    }
                }
            }
        }

        void ArrangeImageGrid(Image newImage)
        {
            int children = imageGrid.Children.Count;

            //Add tapGesture
            if (newImage != null)
            {
                TapGestureRecognizer tap = new TapGestureRecognizer();
                tap.Command = new Command<Image>(DeleteImage);
                tap.CommandParameter = newImage;
                newImage.GestureRecognizers.Add(tap);
            }

            //newImage.Rotation = 0;
            if (newImage == null)
            {
                imageGrid.Children.Clear();
                if (currentClass.userid == userID)
                    imageGrid.Children.Add(captureButton, 0, 0);
                foreach (string s in currentClass.imagePaths)
                {
                    if (s != "")
                        ArrangeImageGrid(new Image { Source = s });
                }
            }
            else
            {
                int cnt = 0;
                for (int column = 0; column < 2; column++)
                {
                    for (int row = 0; row < 2; row++)
                    {
                        cnt++;
                        if (cnt == children + 1)
                        {
                            if (currentClass.userid == userID)
                                imageGrid.Children.Add(captureButton, row, column); //problem with 4 images?
                            else
                                imageGrid.Children.Add(newImage, row, column);

                            return;
                        }
                        else if(cnt == children)
                        {
                            imageGrid.Children.Add(newImage, row, column);
                        }
                    }
                }

                //If it gets this far there is 4 images and therefor the capture button is no longer needed
                imageGrid.Children.Remove(captureButton);
            }
        }

        private async void TakePicture(ObjectClass item)
        {
            //Maximum numberOfImages images so no more pictures after that
            if (imageGrid.Children.Count >= numberOfImages && imageGrid.Children[0] != captureButton)
                return;
            ///
            /// Enable CAMERA, READ_EXTERNAL_STORAGE and WRITE_EXTERNAL_STORAGE in the Android manifest
            /// 
            MediaFile _mediaFile;
            await CrossMedia.Current.Initialize();

            if(!CrossMedia.Current.IsTakePhotoSupported && !CrossMedia.Current.IsPickPhotoSupported)
            {
                await DisplayAlert("Error", "Photo capture and pick is not supported", "Ok");
                return;
            }
            else
            {
                var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                {
                    /// 
                    /// Without this line it saves to a private folder
                    /// Android Paths are as following
                    /// Album path on computer: {This PC}\{Phone Brand}\Phone\Pictures\{Directory Name}
                    /// Private path on computer: {This PC}\{Phone Brand}\Phone\Android\data\{Package Name found in Android Manifest}\files\Pictures\{Directory Name}
                    /// Note that if the eniter path has been deleted you have to run the app once and then allow access to folders again
                    /// 
                    //SaveToAlbum = true,
                    Name = Guid.NewGuid().ToString() + ".jpg"
                });

                if (file == null)
                    return;

                _mediaFile = file;

                //await DisplayAlert("File Path", "Private path: " + file.Path + "Album Path: " + file.AlbumPath, "Ok");
                item.imagePaths[imageGrid.Children.Count - 1] = file.Path;

                ArrangeImageGrid(new Image { Source = file.Path });
            }
        }

        private async void DeleteItem(int itemIndex)
        {
            bool answer = await DisplayAlert("Delete", "Do you wan't to delete this item?", "Yes", "No");
            
            if (!answer || itemIndex < 0)
                return;

            ToggleLoad();

            ObjectClass item = Items[itemIndex];

            foreach(string tempPath in item.imagePaths)
                if(tempPath != "")
                    DeleteImage(tempPath);

            //Delete from server. Server also handles the removal of images
            ServerController.HttpDeleteObject(item);

            //Remove from list
            Items.Remove(item);

            //Update the local file
            SaveLocally();

            DisplayAlert("Delete", "Item deleted.", "Ok");
            HomePage();
            ToggleLoad();
        }

        private async void DeleteImage(Image imageToDelete)
        {
            bool answer = await DisplayAlert("Delete picture", "Do you wan't to delete this picture?", "Yes", "No");

            if (!answer)
                return;

            ToggleLoad();

            string filepath = imageToDelete.Source.ToString().Split(':')[1].TrimStart(' ');

            //Delete image from server and locally
            DeleteImage(filepath);

            //Delete it from the object and move all images back one step
            for (int i = 0; i < numberOfImages; i++)
            {
                if (currentClass.imagePaths[i] == filepath)
                {
                    //Remove the image from the grid
                    imageGrid.Children.RemoveAt(i);
                    //Reset the filepath
                    currentClass.imagePaths[i] = "";
                    //Move all images so no empty path exsist before image paths
                    while (i < numberOfImages)
                    {
                        if (i == 3)
                        {
                            currentClass.imagePaths[i] = "";
                            break;
                        }
                        else if (currentClass.imagePaths[i + 1] != "")
                        {
                            currentClass.imagePaths[i] = currentClass.imagePaths[i + 1];
                            currentClass.imagePaths[i + 1] = "";
                        }
                        else
                            break;

                        i++;
                    }

                    //Update the local file
                    SaveLocally();

                    //Update the server
                    ServerController.HttpEditObject(currentClass);

                    ArrangeImageGrid(null);
                    break;
                }
            }

            ToggleLoad();
        }

        private void DeleteImage(string path)
        {
            //Return if file does not exsist
            if (!File.Exists(path))
                return;

            //Delete image from server
            FileInfo file = new FileInfo(path);
            ServerController.HttpDeleteImage(file.Name.Split('.')[0]);

            //Delete image locally
            if (File.Exists(path))
                File.Delete(path);
        }

        void ToggleLoad()
        {
            bool state = !loading.IsEnabled;
            loading.IsEnabled = state;
            loading.IsVisible = state;
            loading.IsRunning = state;
        }
    }
}
